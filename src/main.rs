
use hashers::fx_hash::FxHasher64;
use std::hash::BuildHasherDefault;
use std::collections::HashMap;


fn main() {
    let mut data: HashMap<String, u32, BuildHasherDefault<FxHasher64>> = HashMap::default();

    data.insert("un".into(), 1);
    data.insert("deux".into(), 2);
    data.insert("trois".into(), 3);
    data.insert("quatre".into(), 4);
    data.insert("cinq".into(), 5);
    data.insert("six".into(), 6);

    println!("Always same hash function");
    for (k, v) in data.iter() {
        println!("{} -> {}", k, v);
    }

    let mut data: HashMap<String, u32> = HashMap::default();


    println!("Random state hash function");
    data.insert("un".into(), 1);
    data.insert("deux".into(), 2);
    data.insert("trois".into(), 3);
    data.insert("quatre".into(), 4);
    data.insert("cinq".into(), 5);
    data.insert("six".into(), 6);

    for (k, v) in data.iter() {
        println!("{} -> {}", k, v);
    }
    
}
